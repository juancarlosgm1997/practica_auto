$('#btn_Guardar').on('click', function() {
    parsleyValidation = false;
});

//Función de envio de formulario al controlador para el guardado del incidente
$('#formPropietario').parsley().on('field:validated', function() {
    var ok = $('.parsley-error').length === 0;
}).on('form:submit', function(e) {
    e.submitEvent.preventDefault();
    parsleyValidation = true;


    let url = $("#formPropietario").attr("Action");
    let formData = new FormData(document.getElementById("formPropietario"));
    let txt_nombre = CKEDITOR.instances["txt_nombre"].getData();

    try {
        formData.append('propietario[nombre]', txt_nombre);
        swal({
                title: "Información",
                text: "¿Desea guardar el propietario?",
                icon: 'info',
                buttons: ["Cancelar", "Aceptar"],
                closeOnClickOutside: false,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $('#btn_Guardar').hide();

                    try {
                        $.ajax({
                            url: url,
                            type: 'POST',
                            contentType: false,
                            data: formData,
                            processData: false,
                            cache: false,
                            success: function(respuesta) {
                                if (!respuesta.error && typeof respuesta.error != "undefined") {
                                    var n = new Noty({
                                        theme: 'sunset',
                                        type: 'success',
                                        text: respuesta.msj,
                                        callbacks: {
                                            onShow: function() {
                                                setTimeout(function() {
                                                    n.close();
                                                }, 5000);
                                            }
                                        }
                                    }).show();
                                    location.href = base_url + "";
                                } else {
                                    var n = new Noty({
                                        theme: 'sunset',
                                        type: 'error',
                                        text: respuesta.msj,
                                        callbacks: {
                                            onShow: function() {
                                                setTimeout(function() {
                                                    n.close();
                                                }, 5000);
                                            }
                                        }
                                    }).show();

                                    //Desbloquea el bóton de envio de formulario y muestra el spinner
                                    $('#btn_Guardar').show();

                                }
                            },
                            error: function() {
                                var n = new Noty({
                                    theme: 'sunset',
                                    type: 'warning',
                                    text: "No fue posible guardar el registro de canalización",
                                    callbacks: {
                                        onShow: function() {
                                            setTimeout(function() {
                                                n.close();
                                            }, 3000);
                                        }
                                    }
                                }).show();


                                $('#btn_Guardar').show();
                            }
                        })

                    } catch (e) {

                        $('#btn_Guardar').show();
                    }
                }
            });
    } catch (ex) {

    }

    return false;

}).on('field:error', function() {
    //Varifica si la validación del formulario ya fue realizada una vez
    if (!parsleyValidation) {
        parsleyValidation = true;
        //Se realiza scroll al elemento del formulario que no cumplio con las reglas de validación del form
        $([document.documentElement, document.body]).animate({ scrollTop: $(this.$element).offset().top }, 100);
        return false;
    }
});