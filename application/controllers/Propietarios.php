<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Propietarios extends CI_Controller {
	public function __construct(){
		parent :: __construct();
		//Importar modelo
		$this ->load->model('Propietarios_model');
		//Helpers
		$this->load->helper("url");
	}
	 public function index()
	{
		$this->load->view('template/header');
        $this->load->view('propietarios/Propietarios');
        $this->load->view('template/footer');
	}
	//Listar Propietarios
	public function ListarPropietarios()
    {
        $getParameters = $this->input->get();
        $response = [
            'rows' => [],
            'total' => 0
        ];
        $propietario = $this->Propietarios_model->ListaPropietarios($getParameters);
        $response['rows'] = $propietario['rows'];
        $response['total'] = $propietario['total'];
        return $this->output->set_content_type("application/json")->set_output(json_encode($response));
    }
	//Function Registrar
	public function Registrar(){
		$propietario=$this->input->post();
		$resp=$this->Propietarios_model->Registro($propietario);
		return $this->output->set_content_type("application/json")->set_output(json_encode($resp));
	}
	//Función Eliminar Propietario
	public function EliminarPropietario(){
		$id= $this->input->post("id");
		$idProp=base64_decode($id);
		$resp=$this->Propietarios_model->EliminaPropietario($idProp);
		return $this->output->set_content_type("application/json")->set_output(json_encode($resp));
	}
	//Editar Propietario
	public function EditarPropietario($id=null){
		$this->input->post();
        $idPropietario = base64_decode($id);	
        $propietario['propietario']= $this->Propietarios_model->ObtenerPropietario($idPropietario);
		$this->load->view('template/header');
        $this->load->view('propietarios/EditarPropietario', $propietario);
        $this->load->view('template/footer');
	}
	//Actualizar el Propietario
	public function ActualizarPropietario(){
		$propietario = $this->input->post();
        $response=$this->Propietarios_model->ActualizaPropietario($propietario);
        return $this->output->set_content_type("application/json")->set_output(json_encode($response));
	}

}


?>