<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Vehiculos extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		//Importar modelo
		$this->load->model('Vehiculos_model');
		$this->load->model('Marcas_model');
		$this->load->model('Colores_model');
		$this->load->model('Propietarios_model');
		//Helpers
		$this->load->helper("url");
	}
	public function index()
	{
		$this->load->view('template/header');
		$this->load->view('vehiculos/Vehiculos');
		$this->load->view('template/footer');
	}
	//Listar Vehiculos
	public function ListarVehiculos()
	{
		$getParameters = $this->input->get();

		$response = [
			'rows' => [],
			'total' => 0
		];

		$vehiculos = $this->Vehiculos_model->ListaVehiculos($getParameters);
		$response['rows'] = $vehiculos['rows'];
		$response['total'] = $vehiculos['total'];

		return $this->output->set_content_type("application/json")->set_output(json_encode($response));
	}
	//Obtener las marcas para poblar el combobox
	public function ObtenerMarca()
	{
		$this->input->post('id');
		$res = $this->Marcas_model->CargarMarcas();
		return $this->output->set_content_type("application/json")->set_output(json_encode($res));
	}
	//Obtener las colores para poblar el combobox
	public function ObtenerColor()
	{
		$this->input->post('id');
		$res = $this->Colores_model->CargarColor();
		return $this->output->set_content_type("application/json")->set_output(json_encode($res));
	}
	//Obtener los propietarios para poblar el combobox
	public function ObtenerPopietarios()
	{
		$this->input->post('id');
		$res = $this->Propietarios_model->CargarPropietarios();
		return $this->output->set_content_type("application/json")->set_output(json_encode($res));
	}
	public function RegistrarAuto()
    {
        $auto = $this->input->post();
        $res = $this->Vehiculos_model->RegistraAuto($auto);
        return $this->output->set_content_type("application/json")->set_output(json_encode($res));
    }
	//Función Eliminar Auto
	public function EliminarAuto(){
		$id= $this->input->post("id");
		$idAuto=base64_decode($id);
		$resp=$this->Vehiculos_model->EliminaAuto($idAuto);
		return $this->output->set_content_type("application/json")->set_output(json_encode($resp));
	}
		//Editar Auto
		public function EditarVehiculos($id=null){
			$this->input->post();
			$idAuto = base64_decode($id);
			$auto['auto']= $this->Vehiculos_model->ObtenerAuto($idAuto);
			$this->load->view('template/header');
			$this->load->view('vehiculos/EditarVehiculo', $auto);
			$this->load->view('template/footer');
		}
		//Actualizar el Propietario
		public function ActualizarAuto(){
			$auto = $this->input->post();
			$response=$this->Vehiculos_model->ActualizaAuto($auto);
			return $this->output->set_content_type("application/json")->set_output(json_encode($response));
		}

}
?>