<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CI_Controller {
	public function __construct(){
		parent :: __construct();
		//Importar modelo
		$this ->load->model('Reportes_model');
		//Helpers
		$this->load->helper("url");
	}

    public function index(){
        $this->load->view('template/header');
        $this->load->view('reportes/Reportes');
        $this->load->view('template/footer');
        
    }
}
?>