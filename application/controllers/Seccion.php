<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seccion extends CI_Controller {

	function __construct()
    {
        parent::__construct();
    }

	/**
	 * index
	 */
	public function index()
	{
		$this->load->view('template/header');
			$this->load->view('seccion/index');
		$this->load->view('template/footer');
	}

	/**
	 * ver
	 * 
	 * @param string $string
	 */
	public function ver($string = NULL)
	{
		$data = [];
		$data['enlace'] = $string;

		$this->load->view('template/header', $data);
			$this->load->view('seccion/ver');
		$this->load->view('template/footer');
	}
}