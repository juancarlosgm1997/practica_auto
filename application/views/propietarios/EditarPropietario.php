<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form action="" method="post" id="formEditPropietario">
                <div class="form-group">
                    <input type="hidden" class="form-control" name="" id="id" value="<?= $propietario->id ?>" >
                </div>
                <div class="form-group">
                    <label for="">Nombre del Propietario:</label>
                    <input type="text" class="form-control" name="" id="nombrePropietario"
                        value="<?= $propietario->nombres ?>">
                </div>
                <div class="form-group">
                    <label for="">Apellido Paterno:</label>
                    <input type="text" class="form-control" name="" id="apePat" value="<?= $propietario->ap_pat ?>">
                </div>
                <div class="form-group">
                    <label for="">Apellido Materno:</label>
                    <input type="text" class="form-control" name="" id="apeMat" value="<?= $propietario->ap_mat ?>">
                </div>

                <div class="form-group">
                    <label for="nombre">Seleccione su sexo:</label>
                    <select class="form-control" id="selSexo" value="<?= $propietario->sexo ?>">
                        <option value="">Seleccionar Sexo</option>
                        <option value="M">Masculino</option>
                        <option value="F">Femenino</option>
                    </select>
                    <div class="form-group">
                        <div class="form-group">
                            <label for="">Teléfono:</label>
                            <input type="text" class="form-control" name="" id="telefono" value="<?= $propietario->telefono ?>"/>
                        </div>
                        <label for="">Dirección:</label>
                        <input type="text" class="form-control" name="" id="direccion"
                        value="<?= $propietario->direccion ?>">
                    </div>
                    <div class="form-group">
                        <label for="">Correo Eléctronico:</label>
                        <input type="text" class="form-control" name="" id="email" value="<?= $propietario->email ?>">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary" id="editarrProp">Guardar</button>

            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    var baseUrl = '<?= base_url(); ?>';
</script>

<script type="text/javascript" src="<?= base_url() ?>assets/bootstrap-table/bootstrap-table.js"></script>
<script type="text/javascript"
    src="<?= base_url() ?>assets/bootstrap-table/extensions/export/bootstrap-table-export.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/bootstrap-table/locale/bootstrap-table-es-MX.js"></script>
<script type="text/javascript"
    src="<?= base_url() ?>assets/bootstrap-table/extensions/filter-control/bootstrap-table-filter-control.js"></script>
<script src="<?= base_url() ?>assets/bootstrap-table/extensions/cookie/bootstrap-table-cookie.js"></script>

<script src="<?= base_url() . 'assets/sweetalert/sweetalert-2.1.0.js' ?>"></script>

<script src="<?= base_url(); ?>js/propietarios/propietarios.js?v=<?= md5(date("YmdHis")); ?>"></script>