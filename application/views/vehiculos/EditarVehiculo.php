<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form action="" method="post" id="formEditAuto">
            <div class="form-group">
                    <input type="hidden" class="form-control" name="" id="id" value="<?= $auto->id?>">
                </div>
                <div class="form-group">
                    <label for="">VIN Vehicular:</label>
                    <input type="text" class="form-control" name="" id="VIN" placeholder="VIN vehicular" value="<?= $auto->VIN_vehicular?>">
                </div>

                <div class="form-group">
                    <label for="">Número de Placa:</label>
                    <input type="text" class="form-control" name="" id="placa" placeholder="Número de placas" value="<?= $auto->num_placa?>">
                </div>

                <div class="form-group">
                    <label for="nombre">Marca:</label>
                    <select class="form-control" id="cboMarca" value="<?= $auto->marca_id?>">
                        <option value="">Seleccione una marca</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="nombre">Color:</label>
                    <select class="form-control" id="cboColor" value="<?= $auto->color_id?>">
                        <option value="">Seleccione un color</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Modelo:</label>
                    <input type="text" class="form-control" name="" id="modelo" placeholder="Ingrese el Año del modelo" value="<?= $auto->anyo_modelo?>">
                </div>

                <div class="form-group">
                    <label for="">País de Origen:</label>
                    <input type="text" class="form-control" name="" id="pais" placeholder="País de Origen" value="<?= $auto->pais_origen?>">
                </div>

                <button type="submit" class="btn btn-primary" id="editarAuto">Guardar</button>

            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    var baseUrl = '<?= base_url(); ?>';
</script>

<script type="text/javascript" src="<?= base_url() ?>assets/bootstrap-table/bootstrap-table.js"></script>
<script type="text/javascript"
    src="<?= base_url() ?>assets/bootstrap-table/extensions/export/bootstrap-table-export.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/bootstrap-table/locale/bootstrap-table-es-MX.js"></script>
<script type="text/javascript"
    src="<?= base_url() ?>assets/bootstrap-table/extensions/filter-control/bootstrap-table-filter-control.js"></script>
<script src="<?= base_url() ?>assets/bootstrap-table/extensions/cookie/bootstrap-table-cookie.js"></script>

<script src="<?= base_url() . 'assets/sweetalert/sweetalert-2.1.0.js' ?>"></script>

<script src="<?= base_url(); ?>js/vehiculos/vehiculos.js?v=<?= md5(date("YmdHis")); ?>"></script> -->