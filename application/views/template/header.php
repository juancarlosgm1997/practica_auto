<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<!-- <meta name="google-signin-client_id" content="410063102046-1ff47uqbcftdv1ojluc1b3396skrgv5k.apps.googleusercontent.com"> -->
	<title>Plantilla Ventanilla - Irapuato</title>

	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/vendor/bootstrap-3.3.7/css/bootstrap.min.css">

	<!-- Monarch -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/monarch.min.css">

	<!-- Ventanilla -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/ventanilla.min.css">

	<!-- Favicons -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= base_url(); ?>assets/img/icons/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= base_url(); ?>assets/img/icons/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= base_url(); ?>assets/img/icons/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="<?= base_url(); ?>assets/img/icons/apple-touch-icon-57-precomposed.png">
	<link rel="shortcut icon" href="<?= base_url(); ?>assets/img/logotipo-iso.png">

	<!-- JQUERy -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js-core/jquery-core.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js-core/jquery-ui-core.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js-core/jquery-ui-widget.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js-core/jquery-ui-mouse.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js-core/jquery-ui-position.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js-core/transition.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js-core/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js-core/jquery-cookie.js"></script>

	<!-- Noty -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/vendor/noty/themes/sunset.css">
	<link rel="stylesheet" href="<?= base_url(); ?>assets/vendor/noty/noty.css">

</head>
<body>
	<div id="loading">
		<div class="spinner">
			<div class="bounce1"></div>
			<div class="bounce2"></div>
			<div class="bounce3"></div>
		</div>
	</div> <!-- /loading -->

	<div id="ayuda">
		<div class="bg-warning">
			<i class="glyph-icon icon-question-circle"></i> <span>Ayuda</span>
		</div>
	</div> <!-- /ayuda -->

	<div class="top-bar" style="color:white !important">
		<div class="container">
			<div class="elements">
				<div class="b-social">
					<a href="#" target="_blank" class="btn btn-sm bg-facebook" title="Síguenos en Facebook">
						<i class="glyph-icon icon-facebook"></i>
					</a>
					<a href="#" target="_blank" class="btn btn-sm bg-twitter" title="Síguenos en Twitter">
						<i class="glyph-icon icon-twitter"></i>
					</a>
				</div>
				<div class="b-title">
					<p>Lorem Ipsum Dolor</p>
				</div>
				<div class="b-login">
					<div class="igob-signin-c" sistema-id="<?= SISTEMA_TOKEN; ?>"></div>
				</div>
			</div> <!-- /.elements -->
		</div> <!-- /.container -->
	</div> <!-- /.top-bar -->

	<div class="main-header bg-header wow fadeInDown" id="fixed-nav">
		<div class="container">
			<div class="b-sistema-logotipo">
				<a href="<?= base_url(); ?>">
					<?= img('assets/img/digital.png', FALSE, array('width' => 'auto', 'height' => '60px', 'title' => SISTEMA_TEXTO)); ?>
				</a>
			</div>
			<div class="right-header-btn">
				<div id="mobile-navigation">
					<button id="nav-toggle" class="collapsed">
						<span></span>
					</button>
				</div>
			</div>
			<ul class="header-nav">
				<li data-toggle="tooltip" data-placement="bottom">
					<a href="<?= base_url(); ?>Propietarios/index">
						<span>Propietarios</span>
					</a>
					<a href="<?= base_url(); ?>Vehiculos/index">
						<span>Vehiculos</span>
					</a>
					<a href="<?= base_url(); ?>Reportes/index">
						<span>Reportes</span>
					</a>
				</li>
			</ul> <!-- /.header-nav -->
			<ul class="header-nav responsive">
				<li>
					<a href="<?= base_url(); ?>Propietarios/index">
						<span>Propietarios</span>
					</a>
					<a href="<?= base_url(); ?>Vehiculos/index">
						<span>Vehiculos</span>
					</a>
					<a href="<?= base_url(); ?>Reportes/index">
						<span>Reportes</span>
					</a>
				</li>
			</ul> <!-- /.header-nav.reponsive -->
		</div> <!-- .container -->
	</div> <!-- /.main-header -->

	<div class="clearfix"></div>

    <main id="content">