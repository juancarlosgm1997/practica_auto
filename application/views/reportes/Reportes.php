<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form action="" method="post" id="formReporte">
                <div class="form-group">
                    <label for="">Descripcion:</label>
                    <input type="text" class="form-control" name="" id="descripcion" placeholder="Descripcion del Reporte">
                </div>

                <button type="submit" class="btn btn-primary" id="guardarReporte">Guardar</button>

            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="tblPropietarios" class="table table-bordered table-borderless table-sm"></table>
        </div>
    </div>
</div>

<script type="text/javascript">
    var baseUrl = '<?= base_url(); ?>';
</script>

<script type="text/javascript" src="<?= base_url() ?>assets/bootstrap-table/bootstrap-table.js"></script>
<script type="text/javascript"
    src="<?= base_url() ?>assets/bootstrap-table/extensions/export/bootstrap-table-export.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/bootstrap-table/locale/bootstrap-table-es-MX.js"></script>
<script type="text/javascript"
    src="<?= base_url() ?>assets/bootstrap-table/extensions/filter-control/bootstrap-table-filter-control.js"></script>
<script src="<?= base_url() ?>assets/bootstrap-table/extensions/cookie/bootstrap-table-cookie.js"></script>

<script src="<?= base_url() . 'assets/sweetalert/sweetalert-2.1.0.js' ?>"></script>

<script src="<?= base_url(); ?>js/propietarios/propietarios.js?v=<?= md5(date("YmdHis")); ?>"></script> -->