		<section id="bienvenida">
			<div class="wrapper">
				<div class="outer-content"></div>
				<div class="inner-content">
					<div class="container">
						<div class="box">
							<div class="b-text">
								<p class="text-justify">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
								</p>
							</div>
							<div class="b-button">
								<button class="btn btn-lg btn-alt btn-hover btn-info">
									Lorem ipsum dolor <i class="glyph-icon icon-info"></i>
								</button>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</section> <!-- /#bienvenida -->