<?php
class Vehiculos_model extends CI_Model
{

	protected $tablaVehiculo='vehiculos';
    protected $tablaMarca='marca';
protected $tablaColor='color';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
    //Lista Vehiculos
    function ListaVehiculos($getParameters)
    {
        //id,VIN_vehicular,num_placa,marca_id,color_id,anyo_modelo,pais_origen
        $response = [
            'rows' => [],
            'total' => 0,
        ];
        $this->db->select("
        {$this->tablaVehiculo}.id,'',
            {$this->tablaVehiculo}.VIN_vehicular,
            {$this->tablaVehiculo}.num_placa,
            {$this->tablaMarca}.nombre AS marca,
            {$this->tablaColor}.nombre AS color,
            {$this->tablaVehiculo}.anyo_modelo,
            {$this->tablaVehiculo}.pais_origen,
           
        ");
        
        $this->db->from($this->tablaVehiculo);
        $this->db->join($this->tablaMarca, "{$this->tablaMarca}.id = {$this->tablaVehiculo}.marca_id");
        $this->db->join($this->tablaColor, "{$this->tablaColor}.id = {$this->tablaVehiculo}.color_id");
        
        $query= $this->db->get();
        $response['total'] = $query->num_rows();
       
        $this->db->select("
    
        {$this->tablaVehiculo}.id,'',
            {$this->tablaVehiculo}.VIN_vehicular,
            {$this->tablaVehiculo}.num_placa,
            {$this->tablaMarca}.nombre AS marca,
            {$this->tablaColor}.nombre AS color,
            {$this->tablaVehiculo}.anyo_modelo,
            {$this->tablaVehiculo}.pais_origen,
            
        ");
        // $this->db->from();
        $this->db->join($this->tablaMarca, "{$this->tablaMarca}.id = {$this->tablaVehiculo}.marca_id");
        $this->db->join($this->tablaColor, "{$this->tablaColor}.id = {$this->tablaVehiculo}.color_id");
        $this->db->where("{$this->tablaVehiculo}.borrado", '0');
        //Obtiene el limit y offset enviados desde get
        $this->db->limit($getParameters["limit"],$getParameters["offset"]);
        
        $response['rows'] = $this->db->get($this->tablaVehiculo)->result_array();

        return $response;
    }
    //Función para registrar un auto
    function RegistraAuto($auto){
        $resp=$this->db->insert($this->tablaVehiculo, $auto);
        $ultimoId = $this->db->insert_id();
        echo "<pre>";
        print_r($ultimoId);
        die();
        return $resp;
    }
    //Funcion para eliminar un auto
    function EliminaAuto($idAuto){
        $this->db->where('id', $idAuto);
        $resp = $this->db->update($this->tablaVehiculo, ['borrado' => 1]);
        return $resp;
    }
    //Obtener Vehiculo a Editar
	function ObtenerAuto($idAuto)
    {
        $this->db->select("*");
        $this->db->from($this->tablaVehiculo);
        $this->db->where("id", $idAuto);
        $resp = $this->db->get()->row();
        return $resp;
    }
    //Actualizar Vehiculo
	function ActualizaAuto($auto)
    {
		
        $this->db->set($auto);
        $this->db->where('id', $auto['id']);
        $resp = $this->db->update($this->tablaVehiculo, $auto);
        return $resp;
    }
}