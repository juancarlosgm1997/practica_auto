<?php
class Marcas_model extends CI_Model
{

	protected $tablaMarca='marca';
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
    //Cargar las marcas para poblar los combobox
    public function CargarMarcas()
    {
        $this->db->where("borrado", '0');
        $this->db->select("
        {$this->tablaMarca}.nombre,id
    ");
        $this->db->from($this->tablaMarca);
        $resp = $this->db->get()->result_array();
        return $resp;
    }
}

?>