<?php
class Propietarios_model extends CI_Model
{

	protected $tablaPropietario='propietarios';
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	//Listar Propietarios
	function ListaPropietarios($getParameters){
		$response = [
            'rows' => [],
            'total' => 0,
        ];
        $this->db->select("
		{$this->tablaPropietario}.id,
			CONCAT({$this->tablaPropietario}.nombres,' ',{$this->tablaPropietario}.ap_pat,' ',{$this->tablaPropietario}.ap_mat) AS nombre_propietario,
			{$this->tablaPropietario}.sexo,
			{$this->tablaPropietario}.telefono,
			{$this->tablaPropietario}.direccion,
			{$this->tablaPropietario}.email,

        ");
        $this->db->from($this->tablaPropietario);
        $query = $this->db->get();
        $response['total'] = $query->num_rows();
        $this->db->select("
		{$this->tablaPropietario}.id,
		CONCAT({$this->tablaPropietario}.nombres,' ',{$this->tablaPropietario}.ap_pat,' ',{$this->tablaPropietario}.ap_mat) AS nombre_propietario,
		{$this->tablaPropietario}.sexo,
		{$this->tablaPropietario}.telefono,
		{$this->tablaPropietario}.direccion,
		{$this->tablaPropietario}.email,
		
    ");
        $this->db->where("borrado", '0');
        //Obtiene el limit y offset enviados desde get
        $this->db->limit($getParameters["limit"], $getParameters["offset"]);
        $response['rows'] = $this->db->get($this->tablaPropietario)->result_array();
        return $response;
	}
	   //Cargar los propietarios para poblar los combobox
	   public function CargarPropietarios()
	   {
		   $this->db->where("borrado", '0');
		   $this->db->select("
		   CONCAT({$this->tablaPropietario}.nombres,' ',{$this->tablaPropietario}.ap_pat,' ',{$this->tablaPropietario}.ap_mat) AS nombre_propietario,id
	   ");
		   $this->db->from($this->tablaPropietario);
		   $resp = $this->db->get()->result_array();
		   return $resp;
	   }
	//Registrar Propietarios
	function Registro($propietario){
		$resp=$this->db->insert($this->tablaPropietario,$propietario);
		return $resp;
	}
	//Obtener Propietario a Editar
	function ObtenerPropietario($idPropietario)
    {
        $this->db->select("*");
        $this->db->from($this->tablaPropietario);
        $this->db->where("id", $idPropietario);
        $resp = $this->db->get()->row();
        return $resp;
    }
	//Actualizar Propietario
	function ActualizaPropietario($propietario)
    {
		
        $this->db->set($propietario);
        $this->db->where('id', $propietario['id']);
        $resp = $this->db->update($this->tablaPropietario, $propietario);
        return $resp;
    }
	//Eliminar Propietario
	function EliminaPropietario($idPropietario){
		$this->db->where('id', $idPropietario);
        $resp = $this->db->update($this->tablaPropietario, ['borrado' => 1]);
        return $resp;
	}

}

?>