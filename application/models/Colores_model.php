<?php
class Colores_model extends CI_Model
{

	protected $tablaColor='color';
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
     //Cargar los colores para poblar los combobox
     public function CargarColor()
     {
         $this->db->where("borrado", '0');
         $this->db->select("
         {$this->tablaColor}.nombre,id
     ");
         $this->db->from($this->tablaColor);
         $resp = $this->db->get()->result_array();
         return $resp;
     }
}

?>