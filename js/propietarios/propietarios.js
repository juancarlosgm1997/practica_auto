const tabla = $("#tblPropietarios");
let sexo;

$(document).ready(function () {
  CargarTablaPropietarios();
});

function CargarTablaPropietarios() {
  tabla.bootstrapTable({
    toolbar: "#toolbar",
    url: `${baseUrl}Propietarios/ListarPropietarios`,
    height: 0,
    pageSize: 5,
    pagination: true,
    sidePagination: "server",
    search: false,
    showRefresh: true,
    paginationLoop: true,
    pageList: [10, 20, 30, 50, 100, "Todo"],
    clickToSelect: false,
    queryParams: function (params) {
      var objFilter = {};
      params.search = "";
      params.filter = objFilter;
      return params;
    },
    columns: [
      {
        title: "Nombre del Propietario",
        align: "center",
        valign: "middle",
        field: "nombre_propietario",
      },
      {
        title: "Sexo",
        align: "center",
        valign: "middle",
        clickToSelect: false,
        field: "sexo",
      },
      {
        title: "Teléfono",
        align: "center",
        valign: "middle",
        clickToSelect: false,
        field: "telefono",
      },
      {
        title: "Dirección",
        align: "center",
        valign: "middle",
        clickToSelect: false,
        field: "direccion",
      },
      {
        title: "Correo  Electrónico",
        align: "center",
        valign: "middle",
        clickToSelect: false,
        field: "email",
      },
      {
        title: "Acciones",
        align: "center",
        valign: "middle",
        clickToSelect: false,
        field: "id",
        formatter: function (value, row, index) {
          //row.id realiza la consulta por medio del id.
          let btn = `<a href="${baseUrl}Propietarios/EditarPropietario/${btoa(
            row.id
          )}" title="Editar Propieatario" class="btn btn-round btn-azure mr-2" data-toggle="tooltip-${index}" data-placement="left" data-togle="modal"><i class="glyph-icon icon-edit"></i></a>`;
          btn += `<a title="Eliminar Propietario" class="btn btn-round btn-danger mr-2" data-toggle="tooltip-${index}" data-placement="left"><i class="glyph-icon icon-trash"data-id='${btoa(
            row.id
          )}'></i></a>`;
          // btn += `<a href="${base_url}administrador/Empresarias/imprimirFicha/${btoa(row.id)}" target="_blank" title="Imprimir ficha" class="btn btn-round btn-yellow mr-2" data-toggle="tooltip-${index}" data-placement="left"><i class="glyph-icon icon-print"></i></a>`;
          // btn += `<a href="${base_url}administrador/Empresarias/vertarjeta/${btoa(row.id)}" target="_blank" title="tarjeta violeta" class="btn btn-round btn-purple mr-2" data-toggle="tooltip-${index}" data-placement="left"><i class="glyph-icon icon-qrcode"></i></a>`;
          return btn;
        },
      },
    ],
  });
}

function ReiniciarForm() {
  $("#nombrePropietario").val("");
  $("#apePat").val("");
  $("#apeMat").val("");
  $("#selSexo").val("");
  $("#telefono").val("");
  $("#direccion").val("");
  $("#email").val("");
}

// Operaciones CRUD
// Ajax para agregar un registro
$(document).on("submit", "#formPropietario", function (e) {
  e.preventDefault();
  //Obtener los valores a rergistar
  let nombres = $("#nombrePropietario").val();
  let apePat = $("#apePat").val();
  let apeMat = $("#apeMat").val();
  let telefono = $("#telefono").val();
  let direccion = $("#direccion").val();
  let email = $("#email").val();

  swal({
    title: "Información",
    text: "¿Desea Agregar un nuevo Propietario ?",
    icon: "info",
    buttons: ["Cancelar", "Aceptar"],
    closeOnClickOutside: false,
  }).then((willDelete) => {
    if (willDelete) {
      $.ajax({
        method: "post",
        url: `${baseUrl}Propietarios/Registrar`,
        data: {
          nombres: nombres,
          ap_pat: apePat,
          ap_mat: apeMat,
          sexo: sexo,
          telefono: telefono,
          direccion: direccion,
          email: email,
        },
        dataType: "json",
        success: function (response) {
          notificar("Registro Agregado Correctamente", "success", 3000);
          //Función para recargar la tabla
          tabla.bootstrapTable("refresh");
          //Reiniciar Formulario
          ReiniciarForm();
        },
      });
    } else {
      //Reiniciar Formulario
      ReiniciarForm();
    }
  });
});

// <----AJAX para Eliminar un Propietario---->
// AJAX para eliminar registro
$(document).on("click", ".icon-trash", function () {
  //obtener el id del elemento seleccionado
  let id = $(this).data("id");
  console.log(id);

  swal({
    title: "Información",
    text: "¿Desea eliminar el Propietario?",
    icon: "info",
    buttons: ["Cancelar", "Aceptar"],
    closeOnClickOutside: false,
  }).then((willDelete) => {
    if (willDelete) {
      $.ajax({
        method: "post",
        url: `${baseUrl}Propietarios/EliminarPropietario`,
        data: { id: id },
        success: function (response) {
          notificar("Registro Eliminado", "success", 3000);
        },
      });
      //Actualizar la tabla
      $(this).parents("tr").remove();
    }
  });
});

$("#selSexo").change(function (e) {
    e.preventDefault();
    //Por cada elemento seleccionado del combo
    $('#selSexo option:selected').each(function () {
        //Mostrar el valor Seleccionado
        sexo = $("#selSexo").val();
        console.log(sexo);
    });
});

//<-----Actualizar Propietario-->
$(document).on("submit", "#formEditPropietario", function (e) {
    e.preventDefault();
    //Obtener los valores a Editar
    let id= $("#id").val();
    let nombres = $("#nombrePropietario").val();
    let apePat = $("#apePat").val();
    let apeMat = $("#apeMat").val();
    let sexo=$("#selSexo").val();
    
    let telefono = $("#telefono").val();
    let direccion = $("#direccion").val();
    let email = $("#email").val();

  
    swal({
      title: "Información",
      text: "¿Desea Editar el Propietario ?",
      icon: "info",
      buttons: ["Cancelar", "Aceptar"],
      closeOnClickOutside: false,
    }).then((willDelete) => {
      if (willDelete) {
        $.ajax({
          method: "post",
          url: `${baseUrl}Propietarios/ActualizarPropietario`,
          data: {
            id:id,
            nombres: nombres,
            ap_pat: apePat,
            ap_mat: apeMat,
            sexo: sexo,
            telefono: telefono,
            direccion: direccion,
            email: email,
          },
          dataType: "json",
          success: function (response) {
            notificar("Registro Editado Correctamente", "success", 3000);
            //Función para recargar la tabla
            tabla.bootstrapTable("refresh");
            //Reiniciar Formulario
            ReiniciarForm();
          },
        });
      } else {
        //Reiniciar Formulario
        ReiniciarForm();
      }
    });
  });
