const tabla = $('#tblVehiculos');
let marcaId;
let colorId;
let propietarioId;

$(document).ready(function () {
    CargarTablaVehiculos();
    LlenarComboMarcas();
    LlenarComboColores();
    LlenarComboPropietarios();
});

function CargarTablaVehiculos() {
    tabla.bootstrapTable({
        toolbar: '#toolbar',
        url: `${baseUrl}Vehiculos/ListarVehiculos`,
        height: 0,
        pageSize: 5,
        pagination: true,
        sidePagination: "server",
        search: false,
        showRefresh: true,
        paginationLoop: true,
        pageList: [2, 4, 8, 16, 32, 'Todo'],
        clickToSelect: false,
        queryParams: function (params) {
            var objFilter = {};
            params.search = "";
            params.filter = objFilter;
            return params;
        },
        columns: [{
            title: 'VIN Vehicular',
            align: 'center',
            valign: 'middle',
            field: 'VIN_vehicular'
        },
        {
            title: 'Número de Placa',
            align: 'center',
            valign: 'middle',
            clickToSelect: false,
            field: 'num_placa'
        },
        {
            title: 'Marca',
            align: 'center',
            valign: 'middle',
            clickToSelect: false,
            field: 'marca'
        },
        {
            title: 'Color',
            align: 'center',
            valign: 'middle',
            clickToSelect: false,
            field: 'color'
        },
        {
            title: 'Año del Modelo',
            align: 'center',
            valign: 'middle',
            clickToSelect: false,
            field: 'anyo_modelo'
        },
        {
            title: 'País de Origen',
            align: 'center',
            valign: 'middle',
            clickToSelect: false,
            field: 'pais_origen'
        },
        {
            title: 'Acciones',
            align: 'center',
            valign: 'middle',
            clickToSelect: false,
            field: 'id',
            formatter: function (value, row, index) {
                //row.id realiza la consulta por medio del id.
                let btn = `<a href="${baseUrl}Vehiculos/EditarVehiculos/${btoa(row.id)}" title="Editar Vehiculo" class="btn btn-round btn-azure mr-2" data-toggle="tooltip-${index}" data-placement="left"><i class="glyph-icon icon-edit" data-id='${btoa(row.id)}'></i></a>`;
                btn += `<a title="Eliminar Vehiculo" class="btn btn-round btn-danger mr-2" data-toggle="tooltip-${index}" data-placement="left"><i class="glyph-icon icon-trash"data-id='${btoa(row.id)}'></i></a>`;
                // btn += `<a href="${base_url}administrador/Empresarias/imprimirFicha/${btoa(row.id)}" target="_blank" title="Imprimir ficha" class="btn btn-round btn-yellow mr-2" data-toggle="tooltip-${index}" data-placement="left"><i class="glyph-icon icon-print"></i></a>`;
                // btn += `<a href="${base_url}administrador/Empresarias/vertarjeta/${btoa(row.id)}" target="_blank" title="tarjeta violeta" class="btn btn-round btn-purple mr-2" data-toggle="tooltip-${index}" data-placement="left"><i class="glyph-icon icon-qrcode"></i></a>`;
                return btn;
            }
        }
        ]
    })
}
//<-------Operaciones CRUD-------->
// Ajax para agregar un registro
$(document).on('submit', '#formAuto', function (e) {
    //Limpiar Formulario
    let vin = $("#VIN").val();
    let placa = $("#placa").val();
    let marca = $("#cboMarca").val();
    let color = $("#cboColor").val();
    let modelo = $("#modelo").val();
    let pais = $("#pais").val();

    e.preventDefault();
    swal({
        title: "Información",
        text: "¿Desea Agregar un nuevo Vehículo ?",
        icon: 'info',
        buttons: ["Cancelar", "Aceptar"],
        closeOnClickOutside: false,
    }).
        then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    method: 'post',
                    url: `${baseUrl}Vehiculos/RegistrarAuto`,
                    data: {
                        VIN_Vehicular: vin,
                        num_placa: placa,
                        marca_id: marcaId,
                        color_id: colorId,
                        anyo_modelo: modelo,
                        pais_origen: pais
                    },
                    dataType: 'json',
                    success: function (response) {
                        notificar('Registro Agregado Correctamente', "success", 3000);
                        //Función para recargar la tabla
                        tabla.bootstrapTable('refresh');
                        ReiniciarForm();
                    }
                });
            } else {
                ReiniciarForm();
            }

            // location.redirect(`${baseUrl}Generos/Index`); 

        });

})
// AJAX para eliminar registro
$(document).on("click", ".icon-trash", function () {
    //obtener el id del elemento seleccionado
    let id = $(this).data("id");
    console.log(id);
    swal({
      title: "Información",
      text: "¿Desea eliminar el Vehiculo?",
      icon: "info",
      buttons: ["Cancelar", "Aceptar"],
      closeOnClickOutside: false,
    }).then((willDelete) => {
      if (willDelete) {
        $.ajax({
          method: "post",
          url: `${baseUrl}Vehiculos/EliminarAuto`,
          data: { id: id },
          success: function (response) {
            notificar("Registro Eliminado", "success", 3000);
          },
        });
        //Actualizar la tabla
        $(this).parents("tr").remove();
      }
    });
  });
// <--------Actualizar Vehiculo--------->
$(document).on('submit', '#formEditAuto', function (e) {
    //Obtener valores a editar
    let id= $("#id").val();
    let vin = $("#VIN").val();
    let placa = $("#placa").val();
    let marca = $("#cboMarca").val();
    let color = $("#cboColor").val();
    let modelo = $("#modelo").val();
    let pais = $("#pais").val();

    e.preventDefault();
    swal({
        title: "Información",
        text: "¿Desea Editar el Vehículo ?",
        icon: 'info',
        buttons: ["Cancelar", "Aceptar"],
        closeOnClickOutside: false,
    }).
        then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    method: 'post',
                    url: `${baseUrl}Vehiculos/ActualizarAuto`,
                    data: {
                        id:id,
                        VIN_Vehicular: vin,
                        num_placa: placa,
                        marca_id: marcaId,
                        color_id: colorId,
                        anyo_modelo: modelo,
                        pais_origen: pais
                    },
                    dataType: 'json',
                    success: function (response) {
                        notificar('Registro Editado Correctamente', "success", 3000);
                        //Función para recargar la tabla
                        tabla.bootstrapTable('refresh');
                        ReiniciarForm();
                    }
                });
            } else {
                ReiniciarForm();
            }

            // location.redirect(`${baseUrl}Generos/Index`); 

        });

})
//Llenado de combobox
function LlenarComboMarcas() {
    $.post(`${baseUrl}Vehiculos/ObtenerMarca`, { 'id': marcaId, },
        function (data) {
            let marcas = data;
            console.log(marcas);
            //Poblar combobox
            $.each(marcas, function (i, item) {
                $("#cboMarca").append(' <option value="' + item.id + '">' + item.nombre + '</option>');
            });
            $("#cboMarca").change(function (e) {
                e.preventDefault();
                //Por cada elemento seleccionado del combo
                $('#cboMarca option:selected').each(function () {
                    //Mostrar el ID Seleccionado
                    marcaId = $('#cboMarca').val();
                });
            });

        }

    );
}

function LlenarComboColores() {
    $.post(`${baseUrl}Vehiculos/ObtenerColor`, { 'id': colorId, },
        function (data) {
            let colores = data;
            console.log(colores);
            //Poblar combobox
            $.each(colores, function (i, item) {
                $("#cboColor").append(' <option value="' + item.id + '">' + item.nombre + '</option>');
            });
            $("#cboColor").change(function (e) {
                e.preventDefault();
                //Por cada elemento seleccionado del combo
                $('#cboColor option:selected').each(function () {
                    //Mostrar el ID Seleccionado
                    colorId = $('#cboColor').val();
                });
            });

        }

    );
}
function LlenarComboPropietarios() {
    $.post(`${baseUrl}Vehiculos/ObtenerPopietarios`, { 'id': propietarioId, },
        function (data) {
            let prop = data;
            console.log(prop);
            //Poblar combobox
            $.each(prop, function (i, item) {
                $("#cboProp").append(' <option value="' + item.id + '">' + item.nombre_propietario + '</option>');
            });
            $("#cboProp").change(function (e) {
                e.preventDefault();
                //Por cada elemento seleccionado del combo
                $('#cboProp option:selected').each(function () {
                    //Mostrar el ID Seleccionado
                    propietarioId = $('#cboProp').val();
                });
            });

        }

    );
}



function ReiniciarForm() {
    $("#VIN").val('');
    $("#placa").val('');
    $("#cboMarca").val('');
    $("#cboColor").val('');
    $("#modelo").val('');
    $("#pais").val('');
}