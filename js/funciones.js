function validateParsleyCURP(el) { $(el).parsley().validate(); }

function validateParsleyRFC(el) { $(el).parsley().validate(); }

function validateParsleyEmail(el) { $(el).parsley().validate(); }

function validateParsleyCp(el) { $(el).parsley().validate(); }

function validateParsleyPhone(el) { $(el).parsley().validate(); }

function validateParsleyDate(el) { $(el).parsley().validate(); }

function validateParsleyFile(el) { $(el).parsley().validate(); }

function validateParsleyURL(el) { $(el).parsley().validate(); }

window.Parsley.addValidator('curp', {
    validateString: value => validarCurp(value),
    messages: { es: "EL CURP INGRESADO NO ES VÁLIDO." }
});

window.Parsley.addValidator('rfc', {
    validateString: value => validarRfcParsley(value),
    messages: { es: "EL RFC INGRESADO NO ES VÁLIDO." }
});

window.Parsley.addValidator('email', {
    validateString: value => validateEmail(value),
    messages: { es: "EL CORREO ELECTRÓNICO INGRESADO NO ES VÁLIDO." }
});

window.parsley.addValidator('maxFileSize', {
    validateString: function(_value, maxSize, parsleyInstance) {
        var files = parsleyInstance.$element[0].files;
        return files.length != 1 || files[0].size <= maxSize * 1024;
    },
    requirementType: 'integer',
    messages: { es: "VERIFIQUE EL TAMAÑO DEL ARCHIVO." }
});

window.Parsley.addValidator('cp', {
    validateString: value => validateCp(value),
    messages: { es: "EL CÓDIGO POSTAL INGRESADO NO ES VÁLIDO." }
});

window.Parsley.addValidator('phone', {
    validateString: value => validatePhoneNumber(value),
    messages: { es: "EL TELÉFONO INGRESADO NO ES VÁLIDO." }
});

window.Parsley.addValidator('date', {
    validateString: value => validateDate(value),
    messages: { es: "LA FECHA INGRESADA NO ES VÁLIDA." }
});

window.Parsley.addValidator('url', {
    validateString: value => validateURL(value),
    messages: { es: "<div style='color: red;'>El URL ingresado no es válido.</div>" }
});


function notificar(texto = '', tipo = "success", timeout = 1500, layout = "topRight") {
    new Noty({
        theme: 'sunset',
        text: texto,
        type: tipo,
        layout: layout,
        timeout: timeout,

    }).show();
}

function validarRFC(rfc) {

    if (rfc != "") {
        rfc = rfc.toUpperCase()
        const re = /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/;
        var validado = rfc.match(re);
        // console.log("match : "+rfc.match(re));
        if (!validado) //Coincide con el formato general del regex?
        { return false; } else {
            return true
        }
    }
    notificar('EL campo rfc no puede ser vacio', 'error', 4000)
    return;
}


function alAutenticar(igob_user) {
    location.reload();
}

$('.solo-numero').on('input', function() {

    var sele = this.selectionStart;
    var regx = /[^0-9]/gi;
    var char = $(this).val();

    if (regx.test(char)) {
        $(this).val(char.replace(regx, ''));
        sele--;
    }

    this.setSelectionRange(sele, sele);
});

function validarCurp(curp) {
    curp = curp.toUpperCase();
    var re = /^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/,
        validado = curp.match(re);

    if (!validado) //Coincide con el formato general?
        return false;

    //Validar que coincida el dígito verificador
    function digitoVerificador(curp17) {
        //Fuente https://consultas.curp.gob.mx/CurpSP/
        var diccionario = "0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZ",
            lngSuma = 0.0,
            lngDigito = 0.0;

        for (var i = 0; i < 17; i++) { lngSuma = lngSuma + diccionario.indexOf(curp17.charAt(i)) * (18 - i); }

        lngDigito = 10 - lngSuma % 10;
        if (lngDigito == 10) return 0;
        return lngDigito;
    }

    if (validado[2] != digitoVerificador(validado[1])) { return false; }

    return true; //Validado
}

function validarRfcParsley(rfc, aceptarGenerico = true) {
    const re = /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/;
    var validado = rfc.match(re);
    // console.log("match : "+rfc.match(re));

    if (!validado) //Coincide con el formato general del regex?
        return false;

    //Separar el dígito verificador del resto del RFC
    const digitoVerificador = validado.pop(),
        rfcSinDigito = validado.slice(1).join(''),
        len = rfcSinDigito.length,

        //Obtener el digito esperado
        diccionario = "0123456789ABCDEFGHIJKLMN&OPQRSTUVWXYZ Ñ",
        indice = len + 1;
    var suma,
        digitoEsperado;

    if (len == 12) suma = 0
    else suma = 481; //Ajuste para persona moral

    for (var i = 0; i < len; i++)
        suma += diccionario.indexOf(rfcSinDigito.charAt(i)) * (indice - i);

    digitoEsperado = 11 - suma % 11;

    if (digitoEsperado == 11) digitoEsperado = 0;
    else if (digitoEsperado == 10) digitoEsperado = "A";

    //El dígito verificador coincide con el esperado?
    // o es un RFC Genérico (ventas a público general)?
    if ((digitoVerificador != digitoEsperado) &&
        (!aceptarGenerico || rfcSinDigito + digitoVerificador != "XAXX010101000"))
        return false;
    else if (!aceptarGenerico && rfcSinDigito + digitoVerificador == "XEXX010101000")
        return false;
    return rfcSinDigito + digitoVerificador;
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function validateCp(cp) {
    var re = /^\d{5}$/;
    return re.test(cp);
}

function validatePhoneNumber(cp) {
    var re = /^\d{10}$/;
    return re.test(cp);
}

function validateDate(fecha) {
    // const re = /^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/i;
    const re = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;

    if (!fecha.match(re)) //Coincide con el formato general del regex?
    { return false; } else { return true; }
}

function validateURL(url) {
    var re = /^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))$/;
    return re.test(String(url).toLowerCase());
}

$(".changeStep").on("click", function()
{
    let tab = $(this).attr("target");

    $('li').removeClass('active');
    $('div').find('.active').removeClass('active');
    $('div').find('.in').removeClass('in');
    $(`.${tab}`).addClass('active in');
    $(`#${tab}`).click();
    $(`li#${tab}`).addClass('active');

    $([document.documentElement, document.body]).animate({ scrollTop: $('.panel-body').offset().top }, 500);
});